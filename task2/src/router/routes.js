
const routes = [
  {
    path: "/",
    redirect: {
      name: "list-user",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "list-user",
        name: "list-user",
        component: () => import("pages/ListOfUser.vue"),
      },
      {
        path: "add-user",
        name: "add-user",
        component: () => import("pages/AddUser.vue"),
      },


    ],
  },

]

export default routes
